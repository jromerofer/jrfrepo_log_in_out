var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
app.listen(port); // Levantamos el servidor para escuchar en un puerto
console.log("API escuchando en el puerto :" + port);

// HELLO registro de ruta de API
app.get('/apitechu/v1/hello',
function(req, res){ // funcion manejadora
console.log("GET /apitechu/v1/hello");
res.send({"msg": "Hola desde APITechU !!"});
}
)

//GET USERS
app.get('/apitechu/v1/users',
function(req, res){ // funcion manejadora
console.log("GET /apitechu/v1/users");
//res.send({"msg": "USUARIOS !!"});
//res.sendFile('usuarios.json', {root: __dirname});
var users = require('./usuarios.json');
res.send(users);
}
)

//POST USERS
app.post('/apitechu/v1/users',
function(req, res){ // funcion manejadora
console.log("POST /apitechu/v1/users");
console.log(req.headers);
}
)
