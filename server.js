//APITechU JRF

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser =require('body-parser'); // libreria para utilizar body en la API
app.use(bodyParser.json());
app.listen(port); // Levantamos el servidor para escuchar en un puerto
console.log("API escuchando en el puerto: " + port + " BBBEEEEEEPPP");

// GET HELLO registro de ruta de API
app.get('/apitechu/v1/hello',
function(req, res){ // funcion manejadora
console.log("GET /apitechu/v1/hello");
res.send({"msg": "Hola desde APITechU !!"});
}
);

/**
//GET USERS
app.get('/apitechu/v1/users',
function(req, res){ // funcion manejadora
console.log("GET /apitechu/v1/users");
//res.send({"msg": "USUARIOS !!"});
//res.sendFile('usuarios.json', {root: __dirname});
var users = require('./MOCK_DATA.json');
res.send(users);
}
);
**/
// POST
app.post('/apitechu/v1/users',
function(req, res){
console.log("POST /apitechu/v1/users");
console.log("first_name es " + req.body.first_name);
console.log("last_name es " + req.body.last_name);
console.log("email es " + req.body.email);

var newUser = {
"first_name" : req.body.first_name,
"last_name" : req.body.last_name,
"email" : req.body.email,
};

var users = require('./MOCK_DATA.json');
users.push(newUser);
writeUserDataToFile(users);

console.log("Usuario añadido con éxito");
res.send({"msg": "Usuario añadido con éxito"});
}
);


//POST MONSTRUO

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res){
   console.log("POST /apitechu/v1/monstruo/:p1/p2");

    console.log("Parámetros");
    console.log(req.params);

    console.log("Query");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);




// DELETE
app.delete("/apitechu/v1/users/:id",
  function(req, res) {

  console.log("DELETE /apitechu/v1/users");
  var users = require('./MOCK_DATA.json');

/** solución del profe:
for (user of users)
  console.log("Length of array is " + users.length);
  if (user |= null && user.id 00 req.params.id) {
    console.log("La id coincide");
    delete users [user.id - 1];
    break
    }
  }
**/

/** solucion del profe con for in
for arrayId in users) {
  console.log("Posición del array  " + arrayId);

if (user[arrayID].id == req.params.id) {
  console.log("La id coincide");
  users.splice(arrayID, 1);
  break
    }
  }

**/

/** solucion del profe con for Each
users.forEach(function (user, index) {
     if (user.id == req.params.id) {
       console.log("La id coincide");
       users.splice(index, 1);
     }
   });
**/


  //Juan del: FOR OF
  var index = 0;
  for (var value of users) {
    console.log(value.id);
    if (req.params.id == value.id) {
      console.log("ID de usuario a borrar encontrada " + value.id + "-->" + req.params.id);
      // para ver la posición usar el índice
      users.splice(index,1);
      console.log("Usuario borrado en el Array");
      writeUserDataToFile(users);

      console.log("Usuario borrado con éxito en fichero");
      res.send({"msg": "Usuario borrado con éxito en fihero"});

      break;
     }
      else {
            index ++;
      }
  }
  }
);



//(Mini)PRÁCTICA
//GET USERS USANDO Query
// OJOOOO si la ruta es la misma del "app.get" solo puede estar activa una. Por eso está asteriscado el de arriba
/**
app.get("/apitechu/v1/users",
 function(req, res) {
   console.log("GET /apitechu/v1/users");
   console.log(req.query);

   var result = {};
   var users = require('./MOCK_DATA.json');

   if (req.query.$count == "true") {
     console.log("Count needed");
     result.count = users.length;
   }

   result.users = req.query.$top ?
      users.slice(0, req.query.$top) : users;

   res.send(result);
 }
);

**/

//PRÁCTICA LOGIN
//

app.post("/apitechu/v1/users/login",
  function(req, res) {
   console.log("POST /apitechu/v1/users/login");
   console.log("BODY pasado en llamada POSTMAN:");
   console.log(req.body);
   console.log(req.body.email);
   console.log(req.body.password);

   var users = require('./users_pwds.json');
   var resultado = {};

    for (var user of users) {

      console.log("ID del usuario: " + user.id);
      console.log("email del usuario: " + user.email);
      console.log("pasword del usuario: " + user.pasword); //OJO el fichero tiene el texto "pasword" no "password"
                                                           //Lo cambio aquí para no rehacer todo el ficherito...

      if (user.email == req.body.email && user.pasword == req.body.password) {

         console.log("Email y Pass de usuario para LOGIN correcto " + user.email + " " + user.pasword);

         user.logged = true;
         console.log("Logged de usuario a: " + user.logged );

         writeUserDataToFile(users);

         console.log("Usuario persistido con logged con éxito en fichero");

         resultado.msg = "Login correcto";
         resultado.idUsuario = user.id;

         // aqui no puedo hacer el res.send({"msg": "Login correcto"}); porque se iría de la ejecución, hay que ponerlo después.
         break;

      } else {
            console.log("No es posible validar el usuario");
            resultado.msg = "Login incorrecto";
      }

    }

    res.send(resultado);

  }

 );


//PRÁCTICA LOGOUT
//

app.post("/apitechu/v1/users/logout",
   function(req, res) {
    console.log("POST /apitechu/v1/users/logout");
    console.log("BODY pasado en llamada POSTMAN:");
    console.log(req.body);
    console.log(req.body.id);

    /** Comentado para evitar el undefined de la variable dado que no va en el BODY
    console.log(req.body.email);
    console.log(req.body.password);
    **/

    var users = require('./users_pwds.json');
    var resultado = {};

     for (var user of users) {

      console.log("ID del usuario: " + user.id);
      console.log("email del usuario: " + user.email);
      console.log("Pasword del usuario: " + user.pasword);

      // El ejecicio solo pide validar el "ID" pero creo que deberían validarse más parémetros... en el BODY, en este caso solo va el ID.
      // Dejo una posible valicación... OJO con la llave ;-) "{"

      //if (user.email == req.body.email && user.pasword == req.body.password && user.id == req.body.id) {
       if (user.id === req.body.id) {

          console.log("ID de usuario para LOGOUT correcto " + user.id);

          delete user.logged;
          console.log("Eliminado Logged de usuario ID: " + user.id);

          writeUserDataToFile(users);

          console.log("Usuario persistido sin logged con éxito en fichero");
          resultado.msg = "Logout correcto";
          resultado.idUsuario = user.id;

          break;

        } else {
            console.log("Logout incorrecto. Necesaria id de usuario");
            resultado.msg = "Logout incorrecto";
            // aqui no puedo hacer el res.send({"msg": "Usuario incorrecto"}); porque se iria de la ejecución hay que ponerlo despues.
          }

      }

     res.send(resultado);

    }
);


// ************************************************ //
// FUNCION DE ESCRITURA
// ************************************************ //

function writeUserDataToFile(data) {
  const fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  //fs.writeFile("./MOCK_DATA.json", jsonUserData, "utf8",
  fs.writeFile("./users_pwds.json", jsonUserData, "utf8",
    function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("Datos escritos en el fichero.")
      }
    }
  )
};
